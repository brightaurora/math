# -*- coding: utf-8 -*-
import numpy as np
import random

##
class FeedforwardError(Exception):
    """Class for exceptions in this module.
    
    Attributes
    ----------
    message : str
        Explanation of the error
    """
    def __init__(self, message):
        """ """
        self.message = message
        
def shuffle(*args, noshuffle=False):
    """Shuffle one or more array_like objects.
    
    The shuffling uses a common permutation for all objects given as parameter.
    
    Parameters
    -----------
    *args : array_like
        Variable number of array_like objects
    noshuffle : bool, optional
        If set to `True` no shuffling is done, default is `False`
    
    Returns
    -------
    list of array_like
        The list of shuffled objects
    """
    if noshuffle:
        return args
    if len(args) > 0:
        p = np.random.permutation(np.arange(len(args[0])))
        return [arg[p] for arg in args]
    
def mean_square_error(target, result, **kwargs):
    """Calculate the mean square error.
    
    For arrays :math:`(t_i)_{i=1..n}=` `target` and :math`(r_i)_{i=1..n}=` `result` calculate
    
    .. math:: \frac{1}{2n}\sum_i (t_i - r_i)^2
    
    Parameters
    ----------
    target, result : array_like
        The arrays to compare. They have to be of the same size and dimension.
    **kwargs 
        Keyword arguments passed to `np.mean`.
    
    Returns
    -------
    float or ndarray
        The mean square error. 
        The type depends on the type returned by `np.mean`.
    """
    if target.shape != result.shape:
        raise FeedforwardError('Different size of target and result.')
    return np.mean((target - result) ** 2, **kwargs) / 2
    
def cross_entropy_error(target, result, **kwargs):
    """Calculate the cross entropy error.
    
    For arrays :math:`(t_i)=` `target` and :math`(r_i)=` `result` calculate
    
    .. math:: -\frac{1}{n}\sum_i\left(t_i\ln(r_i)+(1-t_i)\ln(1-r_i)\right)
    
    Parameters
    ----------
    target, result : array_like
        The arrays to compare. They have to be of the same size and dimension.
    **kwargs 
        Keyword arguments passed to `np.mean`.
    
    Returns
    -------
    float or ndarray
        The cross entropy error. 
        The type depends on the type returned by `np.mean`.
    """
    if target.shape != result.shape:
        raise FeedforwardError('Different size of target and result.')
    return -np.mean(target * np.fmax(-1e100, np.log(result))
        + (1 - target) * np.fmax(-1e100, np.log(1 - result)), **kwargs)
    
class Activation:
    """Base class for activation functions."""
    @staticmethod
    def f(x):
        """Calculate the output activation.
        
        Parameters
        ----------
        x : float or array_like
            The input for the activation function
        
        Returns
        -------
        float or ndarray
            The resulting activation
        """
        pass
    @staticmethod
    def df(x, fx):
        """Calculate the derivative of the activation function.
        
        Parameters
        ----------
        x : float or array_like
            The input for the activation function
        fx : float or array_like
            The output for the activation function
        
        Returns
        -------
        float or ndarray
            The derivative of the activation function at `x`
        """
        pass
    
class SigmoidActivation(Activation):
    """Sigmoid activation function.
    
    The `sigmoid` activation function is defined by
    
    .. math:: \text{sig}(x) = \frac{1}{1+e^{-x}}
    
    with derivative
    
    .. math:: \text{sig}'(x) = \text{sig}(x)\left(1-\text{sig}(x)\right)
    """
    @staticmethod
    def f(x):
        return np.where(x < -45, 0.0 , np.where(x > 45, 1.0, 1.0 / (1.0 + np.exp(-x))))
    @staticmethod
    def df(x, fx):
        return np.fmax(0.0000001, fx * (1.0 - fx))
        
class TanhActivation(Activation):
    """Hyperbolic tangens activation function.
    
    The :math:`tanh` activation function has the derivative
    
    .. math:: \tanh'(x) = \left(1+\tanh(x)\right)\left(1-\tanh(x)\right)
    """
    @staticmethod
    def f(x):
        return np.tanh(x)
    @staticmethod
    def df(x, fx):
        return np.fmax(0.0000001, 1.0 - fx ** 2)    
        
class LinearActivation(Activation):
    """Linear activation function."""
    @staticmethod
    def f(x):
        return x
    @staticmethod
    def df(x, fx): 
        return np.ones(x.shape)
        
class SoftmaxActivation(Activation):
    """Softmax activation function.
    
    The `softmax` activation function of a vector :math:`(x_i)` is defined by
    
    .. math:: \text{softmax}(x)_j = \frac{e^{x_j}}{\sum_i e^{x_i}}
    
    with derivative
    
    .. math:: \frac{\partial\text{softmax}}{\partial x_j}(x)_j = \text{softmax}(x)_j\left(1-\text{softmax}(x)_j\right)
    """
    @staticmethod
    def f(x):
        y = np.exp(x)
        return y / np.sum(y, axis=-1)
    @staticmethod
    def df(x, fx):
        return np.fmax(0.0000001, fx * (1.0 - fx))
    
class Layer:
    """One layer of a feedforward network.
    
    The layer consists of a weight matrix :math:`(w_{ij})`for connections, 
    a vector of biases :math:`b_j` and 
    an activation functio :math:`a` 
    to map input activations :amth:`x_i` to 
    output activations :math:`y_j` according to
    
    .. math: y_j = a\left(\sum_i w_{ij} x_i + b_j\right) 
    
    Attributes
    ----------
    inputsize : int
        Number of inputs, i.e. size of input vector used for activation.
    outputsize : int
        Number of outputs respectively neurons.
    activation : class
        The activation function is a subclass of the `Activation` class
        implementing the static methods `f` and `df`.
    costfunction : function
        Cost function used, if the layer is an output layer of the network.
        `mean_square_error` is the default cost function.
    weight : ndarray
        Connection weights. A numpy array with shape `(outputsize, inputsize)`. 
    bias : ndarray
        Biases. A numpy array with shape `(outputsize,)`.
    deltaweight: ndarray
        A numpy array of shape (..., outputsize, inputsize).
        Derivatives of the cost function with respect to the weight matrix.
        The derivatives are initialized to zero by `start_backpropagation` and
        summed for all subsequent backpropagation steps. 
    deltabias: ndarray
        A numpy array of shape (..., outputsize).
        Derivatives of the cost function with respect to the vector of biases.
        The derivatives are initialized to zero by `start_backpropagation` and
        summed for all subsequent backpropagation steps. 
        
    Note
    ----
        When changing the cost function for an output layer, 
        the `bachpropagate` method must be altered accordingly. 
        
        Use the class `CrossEntropyOutputLayer`for an output layer with
        cross entropy cost function. 
        
        Use the class `SoftmaxOutputLayer` for an output layer
        with `softmax` activation function and cross entropy
        cost function.
         
    """
    def __init__(self, inputsize, outputsize, *args, **kwargs):
        """Constructor.
        
        Parameters
        ----------
        inputsize, outputsize: int
            The number of input connections respectively output neurons of 
            this layer.
        *args
            Other positional arguments
        **kwargs
            Keyword parameters including
            
            activation : class
                A subclass of the `Activation` class implementing the static 
                methods `f` and `df`.
            costfunction: function
                A cost function. The default is `mean_squared_error` and shall
                not be changed directly. Use subclasses of this class for
                different cost functions instead.
            weight: ndarray
                A numpy array with shape (outputsize, inputsize) to 
                initialize weights
            bias: ndarray
                A numpy array with shape (outputsize,) to initialize biases
            
        """
        self.inputsize = inputsize
        self.outputsize = outputsize
        self.activation = SigmoidActivation
        self.costfunction = mean_square_error
        self.weight = np.random.normal(size=(outputsize,inputsize),
            scale=1 / np.sqrt(inputsize))
        self.bias = np.random.normal(size=(outputsize,))
        self.set_options(**kwargs)
            
    def set_options(self, **kwargs):
        """Set attributes by keyword."""
        for k, v in kwargs.items():
            setattr(self, k, v)
        
    def activate(self, input):
        """Activate the layer with an input vector.
        """
        self.input = input
        self.sum = np.einsum('jk,...k->...j',self.weight, input) + self.bias
        self.output = self.activation.f(self.sum)
        return self.output
        
    def start_backpropagation(self):
        """Initialize layer for a new batch."""
        self.deltabias = np.zeros_like(self.sum)
        self.deltaweight = np.zeros(shape=self.sum.shape + (self.inputsize,))
    
    def backpropagate(self, error):
        """Eackpropagate error."""
        self.delta = error * self.activation.df(self.sum, self.output)
        self.deltabias += self.delta
        self.deltaweight += np.einsum('...j,...k->...jk', self.delta, self.input)
        return np.einsum('...j,...jk->...k', self.delta, self.weight)
        
    def count_weights(self):
        """Return number of weights, i.e size of weight matrix."""
        return self.inputsize * self.outputsize
         
class CrossEntropyOutputLayer(Layer):
    """Output layer with cross entropy cost function."""
    def __init__(self, inputsize, outputsize, *args, **kwargs):
        """constructor."""
        super().__init__(inputsize, outputsize, 
            costfunction=cross_entropy_error, *args, **kwargs)
    
    def backpropagate(self, error):
        """Eackpropagate error."""
        self.delta = error
        self.deltabias += self.delta
        self.deltaweight += np.einsum('...j,...k->...jk', self.delta, self.input)
        return np.einsum('...j,...jk->...k', self.delta, self.weight)
        
class SoftmaxOutputLayer(CrossEntropyOutputLayer):
    """Output layer with `softmax` activation function and cross
    entropy cost function.
    """
    def __init__(self, inputsize, outputsize, *args, **kwargs):
        super().__init__(inputsize, outputsize, 
            activation=SoftmaxActivation, *args, **kwargs)
    
class Network:
    """Feedforward neural network.
    """
    def __init__(self, *args, **kwargs):
        """Constructor.
        
        Parameters
        ----------
        *args : Layer
            The layers from which the network will be constructed.
        **kwargs
            Keyword arguments.
        """
        self.layers = list(args)
        self.set_options(**kwargs)
        
    def set_options(self, **kwargs):
        """Set attributes by keyword."""
        for k, v in kwargs.items():
            setattr(self, k, v)
            
    def set_layer_options(self, **kwargs):
        """Set attributes of all layers by keyword."""
        for layer in self.layers:
            layer.set_option(**kwargs)
    
    def activate(self, input):
        """Activate the network by activating each layer."""
        output = np.array(input)
        for layer in self.layers:
            output = layer.activate(output)
        return output
        
    def calculate_cost(self, input, target):
        """Calculate the cost for an input target pair."""
        return np.mean(self.layers[-1].costfunction(target, self.activate(input)))
        
    def start_backpropagation(self):
        """Initialize each layer for a new batch."""
        for layer in self.layers:
           layer.start_backpropagation()
    
    def backpropagate(self, error):
        """Backpropagate the error backwards trough all layers."""
        err = error
        for layer in reversed(self.layers):
            err = layer.backpropagate(err)
        return err
        
    def count_weights(self):
        """Return the number of weights of the whole network."""
        return sum(layer.count_weights() for layer in self.layers)
        
def l2_weight_decay(weight):
    return weight / 2.0
    
def l1_weight_decay(weight):
    return np.sign(weight)
    
class SGD_Trainer:
    """Sochstic gradient descent.
    """
    def __init__(self, network, **kwargs):
        self.net = network
        self.shuffle = True
        self.verify = None
        self.learning_rate = 0.1
        self.decay = 0.01
        self.decay_type = None
        self.set_options(**kwargs)
        
    def set_options(self, **kwargs):
        for k, v in kwargs.items():
            setattr(self, k, v)
        
    def learn(self, layer, nabla_weight, nabla_bias):
        layer.weight -= self.learning_rate * nabla_weight
        layer.bias -= self.learning_rate * nabla_bias
        
    def start_batch(self):
        pass    
        
    def train_batch(self, batchinput, batchtarget):
        self.start_batch()
        output = self.net.activate(batchinput)
        self.net.start_backpropagation()
        self.net.backpropagate(output - batchtarget)
        nweights = self.net.count_weights()
        for layer in self.net.layers:
            nabla_bias = np.mean(layer.deltabias,
                tuple(range(layer.deltabias.ndim-1)))
            nabla_weight = np.mean(layer.deltaweight,
                tuple(range(layer.deltaweight.ndim-2)))
            if self.decay_type:
                nabla_weight += self.decay_type(layer.weight) / nweights
            self.learn(layer, nabla_weight, nabla_bias)
            
    def train(self, traininginput, trainingtarget, epochs=1, batch_size=None, **kwargs):
        self.set_options(**kwargs)
        n = len(traininginput)
        no_shuffle = not self.shuffle
        if batch_size is None:
            batch_size = n
            no_shuffle = True
        cost = []
        for j in range(epochs):
            ti, tt = shuffle(traininginput, trainingtarget, noshuffle=no_shuffle)
            for k in range(0, n, batch_size):
                self.train_batch(ti[k:k+batch_size], tt[k:k+batch_size])
            if self.verify:            
                cost.append(self.calculate_cost(*self.verify))
        return cost
    
    def calculate_cost(self, input, target):
        return self.net.calculate_cost(input, target)
        
class Momentum_Trainer(SGD_Trainer):
    """Stochastic gradient descent with momentum.
    """
    def __init__(self, network, **kwargs):
        self.momentum = 0.9
        super().__init__(network, **kwargs)
        for layer in self.net.layers:
            layer.momentum_weight_velocity = np.zeros_like(layer.weight)
            layer.momentum_bias_velocity = np.zeros_like(layer.bias)
        
    def learn(self, layer, nabla_weight, nabla_bias):
        layer.momentum_weight_velocity = ( 
            self.momentum * layer.momentum_weight_velocity +
            self.learning_rate * nabla_weight)
        layer.weight -= layer.momentum_weight_velocity
        layer.momentum_bias_velocity = (
            self.momentum * layer.momentum_bias_velocity + 
            self.learning_rate * nabla_bias)
        layer.bias -= layer.momentum_bias_velocity
            
class NAG_Trainer(Momentum_Trainer):
    """Nesterov's accelerated gradient method.
    """
    def start_batch(self):
        for layer in self.net.layers:
            layer.nag_last_weight = layer.weight
            layer.weight -= self.momentum * layer.momentum_weight_velocity
            layer.nag_last_bias = layer.bias
            layer.bias -= self.momentum * layer.momentum_bias_velocity
    
    def learn(self, layer, nabla_weight, nabla_bias):
        layer.momentum_weight_velocity = (
            self.momentum * layer.momentum_weight_velocity +
            self.learning_rate * nabla_weight)
        layer.weight = layer.nag_last_weight - layer.momentum_weight_velocity
        layer.momentum_bias_velocity = (
            self.momentum * layer.momentum_bias_velocity + 
            self.learning_rate * nabla_bias)
        layer.bias = layer.nag_last_bias - layer.momentum_bias_velocity

def tests():
    """
    Verify implementation vs step by step example by `Matt Mazur<https://mattmazur.com/2015/03/17/a-step-by-step-backpropagation-example/>`_
    
    >>> hidden = Layer(2, 2, weight=np.array([[0.15, 0.20], [0.25, 0.30]]), bias=np.array([0.35, 0.35]))
    >>> output = Layer(2, 2, weight=np.array([[0.40, 0.45], [0.50, 0.55]]), bias=np.array([0.60, 0.60]))
    >>> net = Network(hidden, output)
    >>> input = np.array([0.05, 0.10])
    >>> net.activate(input)
    array([ 0.75136507,  0.77292847])
    >>> target = np.array([0.01, 0.99])
    >>> output.costfunction(target, net.activate(input))
    0.14918555438000136
    >>> trainer = SGD_Trainer(net, learning_rate=0.5)
    >>> trainer.train(input, target)
    []
    >>> output.weight
    array([[ 0.35891648,  0.40866619],
           [ 0.51130127,  0.56137012]])
    >>> hidden.weight
    array([[ 0.14978072,  0.19956143],
           [ 0.24975114,  0.29950229]])
    """
    pass
    
if __name__ == '__main__':
    import doctest
    doctest.testmod()
    pass
    