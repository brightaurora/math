import numpy as np
import matplotlib.pyplot as plt
import Feedforeward as ff

def generate_net(n=5):
    hidden_layer = ff.Layer(2, n, activation=ff.TanhActivation())
    output_layer = ff.CrossEntropyOutputLayer(n, 3)
    net = ff.Network(hidden_layer, output_layer)
    return net
    
def pol2cart(rho, phi):
    pos = np.array([rho * np.sin(phi), rho * np.cos(phi)])
    return np.transpose(pos)
    
def generate_samples(n):
    dist = np.array([[0.2, 0.3, 0.05], [0.7, 0.4, 0.06], [0.5, 0.8, 0.02]])
    pos = np.array([[1.,1.]])
    for d in distr:
        rho = np.random.exponential(d[2], n)
        phi = np.random.random(n)*np.pi*2.0
        pos = np.concatenate((pos, d[0:2]+pol2cart(rho, phi)), axis=0)
    return (pos[1:], np.reshape(np.tile([[1,0,0],[0,1,0],[0,0,1]],n), (3*n,3)))

n = 20
sample_input, sample_output = generate_samples(n)
plt.clf()
plt.axis([0,1,0,1])
plt.scatter(sample_input[:,0], sample_input[:,1], c=sample_output)

net = generate_net()
dx = 0.05
dy = 0.05
test_input = np.transpose(np.mgrid[slice(0, 1+dx, dx), slice(0, 1+dy, dy)])
test_output = net.activate(test_input)
