#!/usr/bin/python

import sys

def main():
  if len(sys.argv) > 1:
    filename = sys.argv[1]
    with open(filename,'rU') as f:
      for line in f:  
        print line,

if __name__ == '__main__':
  main()
