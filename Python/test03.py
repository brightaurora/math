import numpy as np

class SigmoidActivation():
    """Sigmoid activation function"""
    @staticmethod
    def f(x):
        return np.where(x < -45, 0.0 , np.where(x > 45, 1.0, 1.0 / (1.0 + np.exp(-x))))
    @staticmethod
    def df(x, fx):
        return np.fmax(0.0000001, fx * (1.0 - fx))
      
        
a = SigmoidActivation()

x = np.array([[0, 0],
       [0, 1],
       [1, 0],
       [1, 1]])
       
print(a.f(x))

w = np.random.normal(size=(3,2))
b = np.random.normal(size=(3,))

def activate(input):
    global sum 
    sum = np.einsum('jk,...k->...j',w, x) + b
    return SigmoidActivation.f(sum)
    
y = activate(x)

def mean_square_error(target, result, **kwargs):
    """Calculate the mean square error"""
    if target.shape != result.shape:
        raise FeedforwardError('Different size of target and result.')
    return np.mean((target - result) ** 2, **kwargs) / 2
    

target = np.array([[0,0,0],[1,0,0],[0,1,0],[0,0,1]])

err = target -y

inputsize = 2

def start_backpropagation(error):
    global delta
    global deltabias
    global deltaweight
    deltabias = np.zeros_like(error)
    deltaweight = np.zeros(shape=error.shape+(inputsize,))


def backpropagate(error):
    global delta
    global deltaweight
    global deltabias
    delta = error * SigmoidActivation.df(sum, y)
    deltabias += delta
    deltaweight += np.einsum('...j,...k->...jk', delta, x)
    return np.einsum('...j,...jk->...k', delta, w)

q1 = np.array([[0,0],[0,1],[1,0],[1,1]])
q2 = np.array([[1],[2],[3],[4]])

def s1():
    q1 = np.array([[0,0],[0,1],[1,0],[1,1]])
    q2 = np.array([[1],[2],[3],[4]])
    z = list(zip(q1, q2))
    random.shuffle(z)
    r1, r2 = zip(*z)
    return np.array(r1), np.array(r2)

def s2():
    q1 = np.array([[0,0],[0,1],[1,0],[1,1]])
    q2 = np.array([[1],[2],[3],[4]])
    p = np.random.permutation(np.arange(len(q1)))
    return q1[p], q2[p]
    
import timeit

print(timeit.timeit('s1()', setup='from __main__ import s1'))
print(timeit.timeit('s2()', setup='from __main__ import s2'))

np.random.randint(0, 3, 5)

np.random.exponential(0.05, 5)

distr = np.array([[0.2, 0.3, 0.1], [0.7, 0.5, 0.2], [0.5, 0.8, 0.05]])
for i in distr:
    print(i)