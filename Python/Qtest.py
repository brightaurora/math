import numpy as np
import matplotlib.pyplot as plt
from matplotlib.path import Path
import matplotlib.patches as patches

box1 = [(80.,40.), (90.,40.), (90., 60.), (80., 60.), (0.,0.)]
boxcodes = [ Path.MOVETO, Path.LINETO, Path.LINETO, Path.LINETO, Path.CLOSEPOLY]

boxpath1 = Path(box1, boxcodes)
fig = plt.figure()
patch = patches.PathPatch(boxpath1, facecolor="orange", lw = 2)
ax = fig.add_subplot(111)
ax.add_patch(patch)
ax.set_xlim(0,100)
ax.set_ylim(0,100)
plt.show()
